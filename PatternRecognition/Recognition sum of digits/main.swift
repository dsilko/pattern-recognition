//
//  main.swift
//  Recognition sum of digits
//
//  Created by Denis on 23.07.16.
//  Copyright © 2016 Denis Silko. All rights reserved.
//

import Foundation

print("Example of Bayesian strategy which recognize sum of 20 numerals.")
print("- This program demonstrates Improper, Good and Best Strategies which recognize most likely sum of sequence numerals.\n")

let recognitionSystem = BayesianRecognitionSystem()

let numerals = [7, 4, 1, 3, 2, 5, 6, 9, 8, 2, 4, 1, 7, 6, 5, 2, 0, 4, 2, 7] // 85
let sum = recognitionSystem.recognize(numerals)

print("sum = \(sum), R(q) = \(String(format: "%.2f", recognitionSystem.risk()))\n")