//
//  GoodStrategy.swift
//  PatternRecognition
//
//  Created by Denis on 23.07.16.
//  Copyright © 2016 Denis Silko. All rights reserved.
//

import Foundation

class GoodStrategy: Strategy {
    
    var log = ""
    let states = 0 ... 9
    let signals = 0 ... 9
    let sourceData: SourceData
    
    required init(with sourceData: SourceData) {
        self.sourceData = sourceData
    }
    
    func risk() -> Double {
        return 0.0
    }
    
    func fine(states: [Int], decision: Int) -> Double {
        var sum = 0
        
        for numeral in states {
            sum += numeral
        }
        
        if sum != decision {
            return 1.0
        }
        
        return 0.0
    }
    
    func decisionProbabilities(signals: [Int], index: Int, graphObject: [Double]) -> [Double] {
        var decisionProbabilities = [Double]()
        
        for d in sourceData.decisions {
            var pd = 0.0
            
            // Fj+1
            for k in states {
                if d - k >= 0 {
                    pd += sourceData.probabilitySpace.statePosterioriProbability(k, x: signals[index]) * graphObject[d - k]
                }
            }
            
            decisionProbabilities.append(pd)
        }
        
        return decisionProbabilities
    }

    func recognize(signals: [Int]) -> Int {
        log = "x = \(signals)\n\n"
        log += "Calculate graph consisting of 20 objects ->\n"
        log += "- Graph object corresponds to the index of numeral and represents probabilities of all possible sums.\n"
        log += "- Each successive object uses a 10 probabilities of the last object to calculate the probabilities of all sums.\n"
        log += "- Probabilities correspond to the sequence of numerals which ending on a current index object.\n\n"
        
        var lastGraphObject = [Double]()
        
        // calculate graph
        for i in sourceData.indexes {
            if i > 0 {
                lastGraphObject = decisionProbabilities(signals, index: i, graphObject: lastGraphObject)
            } else { // F1
                for d in sourceData.decisions {
                    lastGraphObject.append(sourceData.probabilitySpace.statePosterioriProbability(d, x: signals[i]))
                }
            }
        }
        
        // argmax p(d*)
        
        log += "Last graph object\n"
        
        var max = 0.0
        var decision = 0
        for (d, p) in lastGraphObject.enumerate() {
            log += "p(d=\(d)) = \(p)\n"

            if max < p {
                max = p
                decision = d
            }
        }
        
        log += "\nargmax p(d=\(decision)) = \(max)\n"
        
        return decision
    }
}