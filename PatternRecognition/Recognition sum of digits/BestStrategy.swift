//
//  BestStrategy.swift
//  PatternRecognition
//
//  Created by Denis on 02.08.16.
//  Copyright © 2016 Denis Silko. All rights reserved.
//

import Foundation

class BestStrategy: Strategy {
    
    var log = ""
    let states = 0 ... 9
    let signals = 0 ... 9
    let sourceData: SourceData
    
    required init(with sourceData: SourceData) {
        self.sourceData = sourceData
    }
    
    func risk() -> Double {
        return 0.0
    }
    
    func fine(states: [Int], decision: Int) -> Double {
        var sum = 0
        
        for numeral in states {
            sum += numeral
        }
        
        return Double(abs(sum - decision))
    }
    
    func recognize(signals: [Int]) -> Int {
        return 0
    }
    
}
