//
//  BayesianRecognitionSystem.swift
//  PatternRecognition
//
//  Created by Denis on 20.07.16.
//  Copyright © 2016 Denis Silko. All rights reserved.
//

import Foundation

class BayesianRecognitionSystem {
    
    let strategy: Strategy
    let sourceData: SourceData
    
    init() {
        sourceData = SourceData()
        print(sourceData.description())
        
        strategy = GoodStrategy(with: sourceData)
    }
    
    func risk() -> Double {
        return strategy.risk();
    }
    
    func recognize(numerals: [Int]) -> Int {
        assert(numerals.count == sourceData.indexes.count,
               "numerals.count = \(numerals.count). Probability space is set for a sequence of \(sourceData.indexes.count) numerals, so max number of numerals should not exceed \(sourceData.indexes.count) units.")
        
        let d = strategy.recognize(numerals)
        print(strategy.log)
        
        return d
    }
    
}