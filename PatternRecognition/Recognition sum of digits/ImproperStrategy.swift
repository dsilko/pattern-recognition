//
//  BestStrategy.swift
//  PatternRecognition
//
//  Created by Denis on 23.07.16.
//  Copyright © 2016 Denis Silko. All rights reserved.
//

import Foundation

class ImproperStrategy: Strategy {
    
    var log = ""
    let states = 0 ... 9
    let signals = 0 ... 9
    let sourceData: SourceData
    
    required init(with sourceData: SourceData) {
        self.sourceData = sourceData
    }
    
    func risk() -> Double {
        var risk = 0.0
        
        for x in signals {
            for k in states {
                let jointP_xk = sourceData.probabilitySpace.signalPrioriProbability(x) *
                                sourceData.probabilitySpace.statePosterioriProbability(k, x: x)
                
                risk += jointP_xk * fine(k, decision: recognize(x).decision)
            }
        }
        
        return risk;
    }
    
    func fine(states: [Int], decision: Int) -> Double { return 0.0 }
    func fine(state: Int, decision: Int) -> Double {
        if state != decision {
            return 1.0
        }
        
        return 0.0
    }
    
    func recognize(signals: [Int]) -> Int {
        log = "x = \(signals)\n"
        
        
        var decision = 0
        
        for x in signals {
            let result = recognize(x)
            
            decision += result.decision
            
            log += "\nargmax p(k*|x=\(x)) = \(result.argmaxP), k = \(result.decision)\n"
        }
        
        return decision
    }
    
    func recognize(signal: Int) -> (decision: Int, argmaxP: Double) {
        var d = 0
        var maxP = 0.0
        
        log += "\nx = \(signal)"
        
        for k in states {
            let p = sourceData.probabilitySpace.statePosterioriProbability(k, x: signal)
            
            log += "\np(k=\(k)|x=\(signal)) = \(String(format: "%.2f", p))"
            
            if p > maxP {
                maxP = p
                d = k
            }
        }
        
        return (d, argmaxP: maxP)
    }
}