//
//  ProbabilitySpace.swift
//  PatternRecognition
//
//  Created by Denis on 23.07.16.
//  Copyright © 2016 Denis Silko. All rights reserved.
//

import Foundation

class ProbabilitySpace {
    
    let signalsPrioriProbabilities =     [0.10, 0.10, 0.10, 0.10, 0.10, 0.10, 0.10, 0.10, 0.10, 0.10]
    
    let statesPosterioriProbabilities = [[0.71, 0.00, 0.01, 0.03, 0.02, 0.01, 0.00, 0.10, 0.08, 0.04],
                                         [0.01, 0.81, 0.00, 0.02, 0.03, 0.01, 0.00, 0.12, 0.00, 0.00],
                                         [0.00, 0.02, 0.91, 0.03, 0.01, 0.00, 0.00, 0.00, 0.01, 0.02],
                                         [0.03, 0.02, 0.00, 0.83, 0.01, 0.02, 0.01, 0.00, 0.06, 0.02],
                                         [0.01, 0.05, 0.02, 0.03, 0.74, 0.00, 0.01, 0.11, 0.02, 0.01],
                                         [0.02, 0.01, 0.00, 0.03, 0.04, 0.87, 0.00, 0.01, 0.02, 0.00],
                                         [0.07, 0.02, 0.01, 0.00, 0.01, 0.02, 0.79, 0.05, 0.02, 0.01],
                                         [0.00, 0.04, 0.03, 0.02, 0.05, 0.02, 0.01, 0.81, 0.01, 0.01],
                                         [0.05, 0.00, 0.02, 0.01, 0.03, 0.01, 0.00, 0.03, 0.81, 0.04],
                                         [0.09, 0.01, 0.02, 0.03, 0.00, 0.01, 0.00, 0.01, 0.02, 0.81]]
    
    func signalPrioriProbability(x: Int) -> Double {
        return signalsPrioriProbabilities[x]
    }
    
    func statePosterioriProbability(k: Int, x: Int) -> Double {
        if k > 9 || k < 0 {
            return 0.0
        }
        
        return statesPosterioriProbabilities[x][k]
    }
    
    func signalPosterioriProbability(x: Int, k: Int) -> Double {
        var pSum = 0.0
        
        for probability in statesPosterioriProbabilities {
            pSum += probability[k]
        }
        
        return statesPosterioriProbabilities[x][k] / pSum
    }
    
    func spaceString(space: [[Double]]) -> String {
        var spaceStr = ""
        
        for x in space {
            var str = ""
            for k in x {
                str += String(format: "%.2f ", k)
            }
            
            spaceStr += "\t" + str + "\n"
        }
        
        return spaceStr
    }
    
    func description() -> String {
        return "\tSignals priori probabilities X\n\(spaceString([signalsPrioriProbabilities])) \n\tStates posteriori probabilities K x X\n\(spaceString(statesPosterioriProbabilities))"
    }
}