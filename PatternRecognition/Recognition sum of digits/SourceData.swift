//
//  SourceData.swift
//  PatternRecognition
//
//  Created by Denis on 23.07.16.
//  Copyright © 2016 Denis Silko. All rights reserved.
//

import Foundation

struct SourceData {
    let indexes = 0 ..< 20
    let decisions = 0 ... 180
    
    let probabilitySpace = ProbabilitySpace()
    
    func description() -> String {
        return "SourceData {\n\tindexes = \(indexes)\n\tdecisions = \(decisions)\n\n\(probabilitySpace.description())}\n"
    }
}