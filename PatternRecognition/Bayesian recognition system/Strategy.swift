//
//  BayesianStrategy.swift
//  PatternRecognition
//
//  Created by Denis on 21.07.16.
//  Copyright © 2016 Denis Silko. All rights reserved.
//

import Foundation

protocol Strategy {
    var log: String { get }
    
    var sourceData: SourceData { get }
    
    init(with sourceData: SourceData)
    func fine(state: Int, decision: Int) -> Double
    func recognize(signal: Int) -> Int
    func risk() -> Double
}