//
//  BayesianRecognitionSystem.swift
//  PatternRecognition
//
//  Created by Denis on 20.07.16.
//  Copyright © 2016 Denis Silko. All rights reserved.
//

import Foundation

class BayesianRecognitionSystem {
    
    let strategy: Strategy
    let sourceData: SourceData
    
    init() {
        sourceData = SourceData()
        print(sourceData.description())
        
        strategy = SimpleStrategy(with: sourceData)
    }
    
    func risk() -> Double {
        return strategy.risk();
    }
    
    func recognize(signal: Int) -> Int {
        assert(sourceData.signals.contains(signal), "Error: Signal can be set from 0 to 9")
        
        let d = strategy.recognize(signal)
        print(strategy.log)
        
        return d
    }
    
}