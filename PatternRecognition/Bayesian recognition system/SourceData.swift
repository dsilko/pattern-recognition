//
//  SourceData.swift
//  PatternRecognition
//
//  Created by Denis on 23.07.16.
//  Copyright © 2016 Denis Silko. All rights reserved.
//

import Foundation

struct SourceData {
    let signals = 0 ... 9
    let states = 0 ... 9
    let decisions = 0 ... 9
    var probabilitySpace = ProbabilitySpace()
    
    func description() -> String {
        return "SourceData {\n\tsignals = \(signals)\n\tstates = \(states)\n\tdecisions = \(decisions)\n\n\(probabilitySpace.description())}\n"
    }
}