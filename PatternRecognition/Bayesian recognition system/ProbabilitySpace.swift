//
//  ProbabilitySpace.swift
//  PatternRecognition
//
//  Created by Denis on 23.07.16.
//  Copyright © 2016 Denis Silko. All rights reserved.
//

import Foundation

struct ProbabilitySpace {
    var prioriProbabilities     =  [0.10, 0.10, 0.10, 0.10, 0.10, 0.10, 0.10, 0.10, 0.10, 0.10]
    
    var posterioriProbabilities = [[0.71, 0.00, 0.01, 0.03, 0.02, 0.01, 0.00, 0.10, 0.08, 0.04],
                                   [0.01, 0.81, 0.00, 0.02, 0.03, 0.01, 0.00, 0.12, 0.00, 0.00],
                                   [0.00, 0.02, 0.91, 0.03, 0.01, 0.00, 0.00, 0.00, 0.01, 0.02],
                                   [0.03, 0.02, 0.00, 0.83, 0.01, 0.02, 0.01, 0.00, 0.06, 0.02],
                                   [0.01, 0.05, 0.02, 0.03, 0.74, 0.00, 0.01, 0.11, 0.02, 0.01],
                                   [0.02, 0.01, 0.00, 0.03, 0.04, 0.87, 0.00, 0.01, 0.02, 0.00],
                                   [0.07, 0.02, 0.01, 0.00, 0.01, 0.02, 0.79, 0.05, 0.02, 0.01],
                                   [0.00, 0.04, 0.03, 0.02, 0.05, 0.02, 0.01, 0.81, 0.01, 0.01],
                                   [0.05, 0.00, 0.02, 0.01, 0.03, 0.01, 0.00, 0.03, 0.81, 0.04],
                                   [0.09, 0.01, 0.02, 0.03, 0.00, 0.01, 0.00, 0.01, 0.02, 0.81]]
    
    func prioriProbability(x: Int) -> Double {
        return prioriProbabilities[x]
    }
    
    func posterioriProbability(k: Int, x: Int) -> Double {
        return posterioriProbabilities[x][k]
    }
    
    func posterioriProbability(x: Int, k: Int) -> Double {
        var pSum = 0.0
        
        for probability in posterioriProbabilities {
            pSum += probability[k]
        }
        
        return posterioriProbabilities[x][k] / pSum
    }
    
    func jointProbability(x: Int, k: Int) -> Double {
        return prioriProbability(x) * posterioriProbability(k, x: x)
    }
    
    func description() -> String {
        return "\(spaceString(prioriProbabilities)) \n\(spaceString(posterioriProbabilities))"
    }
    
    func spaceString(space: [Double]) -> String {
        var spaceStr = "\tPriori probabilities X\n\t"
        
        for x in space {
            spaceStr += String(format: "%.2f ", x)
        }
        
        return spaceStr + "\n"
    }
    
    func spaceString(space: [[Double]]) -> String {
        var spaceStr = "\tPosteriori probabilities K x X \n"
        
        for x in space {
            var str = ""
            for k in x {
                str += String(format: "%.2f ", k)
            }
            
            spaceStr += "\t" + str + "\n"
        }
        
        return spaceStr
    }
}