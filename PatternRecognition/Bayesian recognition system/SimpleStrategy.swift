//
//  SimpleStrategy.swift
//  PatternRecognition
//
//  Created by Denis on 21.07.16.
//  Copyright © 2016 Denis Silko. All rights reserved.
//

import Foundation

class SimpleStrategy: Strategy {
    
    var log = ""
    let sourceData: SourceData
    
    required init(with sourceData: SourceData) {
        self.sourceData = sourceData
    }
    
    func fine(state: Int, decision: Int) -> Double {
        if state != decision {
            return 1.0
        }
        
        return 0.0
    }
    
    func risk() -> Double {
        var risk = 0.0
        
        for x in sourceData.signals {
            for k in sourceData.states {
                risk += sourceData.probabilitySpace.jointProbability(x, k: k) * fine(k, decision: recognize(x))
            }
        }
        
        return risk
    }
    
    // Maximizing strategy. complexity = |k|
    func recognize(signal: Int) -> Int {
        log = "x = \(signal)\n"
        
        var d = 0
        var max = 0.0
        
        for k in sourceData.states {
            let p = sourceData.probabilitySpace.posterioriProbability(k, x: signal)
            
            log += "p(k=\(k)|x=\(signal)) = \(String(format: "%.2f", p))\n"
            
            if p > max {
                max = p
                d = k
            }
        }
        
        log += "\nargmax p(k*|x=\(signal)) = \(max), k = \(d)"
        
        return d
    }
    
    // Minimizing strategy. complexity = |k^2|
    /*
    func recognize(signal: Int) -> Int {
        log = "x = \(signal)\n"
     
        var d = 0
        var min = 1.0
        
        for _d in sourceData.decisions {
            var sum = 0.0
            
            for k in sourceData.states {
                let p = sourceData.probabilitySpace.posterioriProbability(k, x: signal) * fine(k, decision: _d)
                sum += p
            }
            
            log += "sum p(k*|x=\(signal)) * w(k*, d=\(_d)) = \(String(format: "%.2f", sum))\n"
            
            if sum < min {
                min = sum
                d = _d
            }
        }
     
        log += "\nargmin(d) sum p(k*|x=\(signal)) * w(k*, d*) = \(min), k = \(d)\n"
     
        return d
    }
    */
}