//
//  main.swift
//  The minimum configuration and implementation of Bayesian recognition system
//
//  Created by Denis on 16.07.16.
//  Copyright © 2016 Denis Silko. All rights reserved.
//

import Foundation

print("Minimal configuration and implementation of Bayesian recognition system.")
print("- Strategy is determined by the loss function.")
print("- In this example, strategy corresponds to a simplest loss function. w(k, d) = [1, k != d], [0, k == d]\n")

let recognitionSystem = BayesianRecognitionSystem()

let signal = 7 // 0 ... 9
let decision = recognitionSystem.recognize(signal)

print("d = \(decision), R(q) = \(String(format: "%.2f", recognitionSystem.risk()))\n")
